import React from 'react';
import { Route } from 'react-router-dom';
import Login from './pages/login/Login';
import MenuList from './pages/Menu/MenuList';

const Routes = () => {
  return (
    <>
      <Route exact path="/" component={Login} />
      <Route path="/menu-list" component={MenuList} />
    </>
  );
};

export default Routes;
