export const SET_USER_ROLE = 'SET_USER_ROLE';
export const LOGIN_ERROR = 'LOGIN_ERROR';

export const setUserRole = (userName, password) => ({
  type: SET_USER_ROLE,
  userName,
  password
});

export const loginError = error => ({
  type: LOGIN_ERROR,
  error
});
