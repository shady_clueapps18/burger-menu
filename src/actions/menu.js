export const LOAD_MENU_LIST = 'LOAD_MENU_LIST';
export const ADD_CATEGORY = 'ADD_CATEGORY';
export const EDIT_CATEGORY = 'EDIT_CATEGORY';
export const REMOVE_CATEGORY = 'REMOVE_CATEGORY';
export const ADD_MENU_ITEM = 'ADD_MENU_ITEM';
export const EDIT_MENU_ITEM = 'EDIT_MENU_ITEM';
export const REMOVE_MENU_ITEM = 'REMOVE_MENU_ITEM';

export const loadMenuList = list => ({
  type: LOAD_MENU_LIST,
  list
});

export const addCategory = category => ({
  type: ADD_CATEGORY,
  category
});

export const editCategory = (toBeEditedCategory, editValue) => ({
  type: EDIT_CATEGORY,
  toBeEditedCategory,
  editValue
});

export const removeCategory = id => ({
  type: REMOVE_CATEGORY,
  id
});

export const addMenuItem = ({ name, description, price }) => ({
  type: LOAD_MENU_LIST,
  name,
  description,
  price
});

export const removeMenuItem = id => ({
  type: REMOVE_MENU_ITEM,
  id
});
