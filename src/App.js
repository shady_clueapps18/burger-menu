import React, { Component } from 'react';
import Routes from './routes';
import Login from './pages/login/Login';
import AddCategory from './pages/Menu/AddCategory';
import MenuList from './pages/Menu/MenuList';

class App extends Component {
  render() {
    return (
      <Routes>
        <Login />
        <MenuList />
        <AddCategory />
      </Routes>
    );
  }
}

export default App;
