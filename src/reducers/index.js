import { reducer as formReducer } from 'redux-form';
import { combineReducers } from 'redux';
import login from './login';
import menu from './menu';

const rootReducer = combineReducers({
  form: formReducer,
  login,
  menu
});

export default rootReducer;
