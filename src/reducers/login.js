import { SET_USER_ROLE, LOGIN_ERROR } from '../actions/login';
const initialState = {
  errors: {},
  userRole: null
};

export default (state = initialState, action) => {
  const { type, userName, password, error } = action;
  switch (type) {
    case LOGIN_ERROR:
      return {
        ...state,
        errors: { error }
      };

    case SET_USER_ROLE:
      return {
        ...state,
        userRole:
          userName === 'admin' && password === 'admin'
            ? 'ADMIN'
            : userName === 'user' && password === 'user'
              ? 'USER'
              : null
      };

    default:
      return state;
  }
};
