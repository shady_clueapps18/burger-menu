import {
  LOAD_MENU_LIST,
  ADD_CATEGORY,
  EDIT_CATEGORY,
  REMOVE_CATEGORY,
  ADD_MENU_ITEM,
  EDIT_MENU_ITEM,
  REMOVE_MENU_ITEM
} from '../actions/menu';
const initialState = {
  menuList: [],
  toBeEditedCategory: {
    id: null,
    newValue: null
  },
  toBeEditedItem: null
};

export default (state = initialState, action) => {
  const { type, list, category } = action;
  switch (type) {
    case LOAD_MENU_LIST:
      return {
        ...state,
        menuList: list
      };

    case ADD_CATEGORY:
      return {
        ...state,
        menuList: state.menuList.concat(category)
      };

    case REMOVE_CATEGORY:
      return {
        ...state,
        menuList: state.menuList.filter(item => item.id !== action.id)
      };
    default:
      return state;
  }
};
