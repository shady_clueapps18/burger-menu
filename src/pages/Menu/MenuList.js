import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import styled from 'styled-components';
import { Accordion, Icon, Segment, Button } from 'semantic-ui-react';
import { loadMenuList, removeCategory } from '../../actions/menu';
import MenuItem from './MenuItem';
import AddCategory from './AddCategory';
import AddItem from './AddItem';

const AccordionContent = styled(Accordion.Content)`
  &&&&&&&& {
    background: rgba(0, 0, 0, 0.1);
    padding: 1rem 1.5rem;
  }
`;

const AccordionHeader = styled(Accordion.Title)`
  &&&&&&&& {
    padding-bottom: 0;
  }
`;

const CategoryTitle = styled(Segment)`
  &&&&&&&& {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
`;

class MenuList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeIndex: -1
    };
  }

  componentDidMount = () => {
    const userRole = localStorage.getItem('userRole')
      ? localStorage.getItem('userRole')
      : this.props.userRole;

    localStorage.setItem('userRole', userRole);

    if (localStorage.getItem('menu')) {
      const menu = JSON.parse(localStorage.getItem('menu'));
      console.log(menu);
      this.props.loadMenuList(menu);
    } else {
      axios
        .get(`menuList.json`)
        .then(({ data }) => {
          this.props.loadMenuList(data.categories);
          localStorage.setItem('menu', JSON.stringify(data));
        })
        .catch(err => console.error(err));
    }
  };

  componentDidUpdate(prevProps) {
    this.props.menuList !== prevProps.menuList &&
      localStorage.setItem('menu', JSON.stringify(this.props.menuList));
  }

  toggleAccordion = (e, titleProps) => {
    const { index } = titleProps;
    const { activeIndex } = this.state;
    const newIndex = activeIndex === index ? -1 : index;

    this.setState({ activeIndex: newIndex });
  };

  render() {
    const { menuList, userRole, removeCategory } = this.props;
    const { activeIndex } = this.state;
    return (
      <div>
        {userRole === 'ADMIN' && <AddCategory />}
        <Accordion>
          {menuList.map((i, index) => {
            const removeCategoryAction = () => removeCategory(i.id);
            return (
              <>
                <AccordionHeader
                  id={i.id}
                  active={activeIndex === index}
                  index={index}
                  onClick={this.toggleAccordion}
                  key={i.id}
                >
                  <CategoryTitle>
                    <div>
                      <Icon name="bars" />
                      {i.name}
                    </div>
                    {userRole === 'ADMIN' && (
                      <div>
                        <Button color="blue">Edit</Button>
                        <Button color="red" onClick={removeCategoryAction}>
                          Delete
                        </Button>
                      </div>
                    )}
                  </CategoryTitle>
                </AccordionHeader>
                <AccordionContent active={activeIndex === index}>
                  {userRole === 'ADMIN' && <AddItem />}
                  {i.items.map(item => (
                    <MenuItem
                      id={item.id}
                      userRole={userRole}
                      name={item.name}
                      description={item.description}
                      price={item.price}
                      key={item.id}
                    />
                  ))}
                </AccordionContent>
              </>
            );
          })}
        </Accordion>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  userRole: state.login.userRole,
  menuList: state.menu.menuList
});

const mapDispatchToProps = {
  loadMenuList,
  removeCategory
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MenuList);
