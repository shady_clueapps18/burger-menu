import React from 'react';
import { Segment, Header, Button } from 'semantic-ui-react';
import { connect } from 'react-redux';
import styled from 'styled-components';

const MenuItemContainer = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 0 1rem;
`;

const TitlePriceWrapper = styled.div`
  display: flex;
`;

const ItemPrice = styled(Header)`
  &&&&&&& {
    margin-top: 0;
  }
`;

const ItemTitle = styled(Header)`
  &&&&&&& {
    margin: 0 1.5rem 0 0;
  }
`;

const ItemActions = styled.div`
  display: flex;
`;

const MenuItem = ({ id, name, description, price, userRole }) => {
  return (
    <Segment>
      <MenuItemContainer>
        <div>
          <TitlePriceWrapper>
            <ItemTitle>{name}</ItemTitle>
            <ItemPrice>{price.toFixed(2)} EGP</ItemPrice>
          </TitlePriceWrapper>
          <p>{description}</p>
        </div>
        {userRole === 'ADMIN' && (
          <ItemActions>
            <Button color="blue">Edit</Button>
            <Button color="red">Delete</Button>
          </ItemActions>
        )}
      </MenuItemContainer>
    </Segment>
  );
};

const mapStateToProps = state => ({
  userRole: state.login.userRole
});

export default connect(mapStateToProps)(MenuItem);
