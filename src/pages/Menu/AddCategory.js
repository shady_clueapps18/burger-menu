import React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Button, Form, Segment, Header } from 'semantic-ui-react';
import { Field, reduxForm, formValueSelector, reset } from 'redux-form';
import { addCategory } from '../../actions/menu';

let AddCategory = ({ handleSubmit, addCategorySelector, addCategory }) => {
  const { englishName, englishDescription } = addCategorySelector;
  const categoryId = Math.floor(Math.random() * 1000000);
  const addCategoryAction = () =>
    addCategory({
      id: categoryId,
      name: englishName,
      description: englishDescription,
      items: []
    });

  const resetAddCategoryForm = reset('addCategory');
  return (
    <Segment>
      <Header>Add New Category</Header>
      <Form onSubmit={handleSubmit(addCategoryAction, resetAddCategoryForm)}>
        <Form.Field>
          <label>English Name</label>
          <Field
            name="englishName"
            component="input"
            placeholder="English Name"
          />
        </Form.Field>
        <Form.Field>
          <label>English Description</label>
          <Field
            name="englishDescription"
            component="textarea"
            placeholder="English Description"
          />
        </Form.Field>
        <Button color="teal" type="submit">
          Add Category
        </Button>
      </Form>
    </Segment>
  );
};

const addCategorySelector = formValueSelector('addCategory');

const mapStateToProps = state => ({
  addCategorySelector: addCategorySelector(
    state,
    'englishName',
    'englishDescription'
  )
});

const mapDispatchToProps = {
  addCategory,
  reset
};

export default compose(
  reduxForm({
    form: 'addCategory'
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(AddCategory);
