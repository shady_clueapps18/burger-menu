import React from 'react';
import { Button, Form, Segment, Header } from 'semantic-ui-react';
import { Field, reduxForm } from 'redux-form';

let AddItem = ({ handleSubmit }) => {
  return (
    <Segment>
      <Header>Add New Item</Header>
      <Form onSubmit={handleSubmit}>
        <Form.Field>
          <label>English Name</label>
          <Field
            name="englishName"
            component="input"
            placeholder="English Name"
          />
        </Form.Field>
        <Form.Field>
          <label>Price</label>
          <Field
            name="price"
            component="input"
            placeholder="Price"
            type="number"
          />
        </Form.Field>
        <Form.Field>
          <label>English Description</label>
          <Field
            name="englishDescription"
            component="textarea"
            placeholder="English Description"
          />
        </Form.Field>
        <Button color="teal" type="submit">
          Add Item
        </Button>
      </Form>
    </Segment>
  );
};

AddItem = reduxForm({
  form: 'addItem'
})(AddItem);

export default AddItem;
