import React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { Form, Message, Button, Segment, Header } from 'semantic-ui-react';
import styled from 'styled-components';
import { setUserRole, loginError } from '../../actions/login';

const StyledGrid = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100vw;
  height: 100vh;
`;

const StyledSegment = styled(Segment)`
  &&&&&& {
    width: 40vw;
  }
`;

let Login = ({
  loginError,
  errors,
  handleSubmit,
  setUserRole,
  history,
  loginSelector
}) => {
  const { userName, password } = loginSelector;
  const validate = () => {
    if (userName === 'user' && password === 'user') {
      return true;
    } else if (userName === 'admin' && password === 'admin') {
      return true;
    }
    return false;
  };

  const verifyUser = () => {
    if (!userName || !password) {
      loginError('You have to enter your user name and password to log in');
      return;
    } else if (!validate()) {
      loginError('You have to enter valid data to log in');
      return;
    } else {
      setUserRole(loginSelector.userName, loginSelector.password);
      return history.push('/menu-list');
    }
  };

  return (
    <StyledGrid>
      <Header>LOGIN</Header>
      <StyledSegment>
        {Object.keys(errors).length > 0 ? (
          <Message color="red">
            <Message.Header>Something went wrong</Message.Header>
            {Object.values(errors)[0]}
          </Message>
        ) : null}
        <Form onSubmit={handleSubmit(verifyUser)}>
          <div>
            <label>User Name</label>
            <Field
              component="input"
              name="userName"
              type="text"
              placeholder="First Name"
            />
          </div>
          <div>
            <label>Password</label>
            <Field
              component="input"
              name="password"
              type="password"
              placeholder="Password"
            />
          </div>
          <Button type="submit">Submit</Button>
        </Form>
        <Message warning>
          <Message.Header>Credentials</Message.Header>
          <p>
            Use User name and password "user" for user login and "admin" for
            admin login
          </p>
        </Message>
      </StyledSegment>
    </StyledGrid>
  );
};

const loginSelector = formValueSelector('login');

const mapStateToProps = state => ({
  loginSelector: loginSelector(state, 'userName', 'password'),
  errors: state.login.errors
});

const mapDispatchToProps = {
  setUserRole,
  loginError
};

export default compose(
  reduxForm({
    form: 'login'
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(withRouter(Login));
